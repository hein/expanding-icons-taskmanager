/*****************************************************************

Copyright 2008 Christian Mollekopf <chrigi_1@hotmail.com>

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.  IN NO EVENT SHALL THE
AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN
AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

******************************************************************/

#include "manualsortingstrategy.h"
#include "taskgroup.h"

#include <QMap>

#include <KDebug>

namespace TaskManager
{

ManualSortingStrategy::ManualSortingStrategy(GroupManager *parent)
    : AbstractSortingStrategy(parent)
{
    setType(GroupManager::ManualSorting);
}

ManualSortingStrategy::~ManualSortingStrategy()
{
}

bool ManualSortingStrategy::manualSortingRequest(AbstractGroupableItem *item, int newIndex)
{
    GroupManager *gm = qobject_cast<GroupManager *>(parent());

    if (!gm || newIndex < 0) {
        return false;
    }

    bool moved = false;
    int currentIndex = gm->rootGroup()->members().indexOf(item);

    if (newIndex <= gm->rootGroup()->members().count()) {
        AbstractGroupableItem *target = gm->rootGroup()->members().at(newIndex > currentIndex ? newIndex - 1 : newIndex);

        if (!same(target, item)) {
            ItemList group;

            foreach(AbstractGroupableItem *groupable, gm->rootGroup()->members()) {
                if (same(groupable, item)) {
                    newIndex < currentIndex ? group.prepend(groupable) : group.append(groupable);
                }
            }

            foreach(AbstractGroupableItem *groupable, gm->rootGroup()->members()) {
                if (same(groupable, target)) {
                    if (newIndex < currentIndex) {
                        newIndex = gm->rootGroup()->members().indexOf(groupable);
                        break;
                    } else {
                        newIndex = gm->rootGroup()->members().indexOf(groupable) + 1;
                    }
                }
            }



            foreach(AbstractGroupableItem *groupable, group) {
                moved = moveItem(groupable, newIndex);
            }
        }
    }

    if (moved) {
        QList<KUrl> launcherUrls;

        foreach(AbstractGroupableItem *groupable, gm->rootGroup()->members()) {
            if (!launcherUrls.contains(groupable->launcherUrl()) &&
                gm->launcherIndex(groupable->launcherUrl()) != -1) {
                launcherUrls.append(groupable->launcherUrl());
            }
        }

        foreach(const KUrl url, launcherUrls) {
            gm->moveLauncher(url, launcherUrls.indexOf(url));
        }
    }

    return moved;


    int oldIndex = gm->launcherIndex(item->launcherUrl());

    if (LauncherItemType == item->itemType() || -1 != oldIndex) {
        bool moveRight = oldIndex > -1 && newIndex > oldIndex;

        if (newIndex >= 0 && newIndex - (moveRight ? 1 : 0) < gm->launcherCount()) {
            if (moveItem(item, newIndex)) {
                gm->moveLauncher(item->launcherUrl(), (newIndex > oldIndex ? --newIndex : newIndex));
                return true;
            }
        }
    } else if (newIndex >= gm->launcherCount()) {
        return moveItem(item, newIndex);
    }

    return false;
}

bool ManualSortingStrategy::same(AbstractGroupableItem* a, AbstractGroupableItem* b) const
{
    if (a->itemType() == TaskItemType && b->itemType() == TaskItemType) {
        TaskItem *aTaskItem = qobject_cast<TaskItem *>(a);
        TaskItem *bTaskItem = qobject_cast<TaskItem *>(b);

        if (aTaskItem->task() && bTaskItem->task()) {
            return (aTaskItem->task()->classClass() == bTaskItem->task()->classClass());
        }
    }

    return (a->launcherUrl() == b->launcherUrl());
}

void ManualSortingStrategy::sortItems(ItemList &items)
{
    ItemList newOrder;
    QHash<QString, int> classInsertIndices;

    foreach(AbstractGroupableItem *groupable, items) {
        if (groupable->itemType() == TaskItemType)
        {
            TaskItem *taskItem = qobject_cast<TaskItem *>(groupable);

            if (taskItem->task()) {
                if (classInsertIndices.contains(taskItem->task()->classClass())) {
                    int index = classInsertIndices[taskItem->task()->classClass()] + 1;
                    newOrder.insert(index, groupable);
                    classInsertIndices[taskItem->task()->classClass()] = index;
                    continue;
                } else {
                    classInsertIndices[taskItem->task()->classClass()] = newOrder.size();
                }
            }
        }

        newOrder.append(groupable);
    }

    items = newOrder;
}

} //namespace
#include "manualsortingstrategy.moc"

